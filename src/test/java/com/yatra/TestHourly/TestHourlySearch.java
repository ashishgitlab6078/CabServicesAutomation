package com.yatra.TestHourly;

import java.util.List;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.yatra.CommonUtils.CommonFunctions;
import com.yatra.HourlySearchRequest.HourlySearchRequestBean;
import com.yatra.HourlySearchRequest.HourlySearchRequestObejct;
import com.yatra.HourlySearchUtils.HourlySearchRequestUtils;
import com.yatra.testDataGenerator.TestDataGenerator;

public class TestHourlySearch {
	
	TestDataGenerator testData=new TestDataGenerator();
	CommonFunctions cfobj=new CommonFunctions();
	
	
	String sheetName="HourlySearch";
	
	/*@BeforeTest
	@Parameters({"sheetname"})
	public void getSheetDetails(String sheetname){
		this.sheetName=sheetname;
	}*/
	
	HourlySearchRequestObejct hourlySearchRequestObejct=new HourlySearchRequestObejct();
	
	@Test(dataProvider="HourlySearchData")
	public void testHourlySearch(List testData)
	{
			
		 String travel_type=testData.get(1).toString();
		 int seats=Integer.parseInt(testData.get(2).toString());
		 String address=testData.get(3).toString();
		 int city_id=Integer.parseInt(testData.get(4).toString());
		 int pickup_date=Integer.parseInt(testData.get(5).toString());
		 int hourly_package_id=Integer.parseInt(testData.get(6).toString());
		 String[] driver_criteria=new String[0];
		 String[] vehicle_criteria=new String[0];
		 
		 long pickup_time=cfobj.getEpochTime(pickup_date);
		
		 HourlySearchRequestBean hourlySearchRequestBean=hourlySearchRequestObejct.hourlySearchRequestObject(travel_type, seats, address, city_id, pickup_time, driver_criteria, vehicle_criteria,hourly_package_id);
		 
		HourlySearchRequestUtils hourlySearchRequestUtils=new HourlySearchRequestUtils();
		String request=hourlySearchRequestUtils.createRequest(hourlySearchRequestBean);
		
		hourlySearchRequestUtils.hourlySearchResultVerifcation(request);
		 
		
	}
	
	@DataProvider(name="HourlySearchData")
	public Object[][] getHourlySearchData() throws Exception {
		Object[][] Hourly = null;
	
		try {

			//if (DataDriver.equals("EXCEL")) {
				Hourly = testData.getTestDataForSanityTest(sheetName);
		//	} else if (DataDriver.equals("OPENEXCEL")) {
				//Hourly = testData.getOOTestDataForSanityTest(sheetName);
			//}
		//	Hourly=testData.getTestDataForSanityTest(sheetName);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Hourly;
	}
	
	

}
