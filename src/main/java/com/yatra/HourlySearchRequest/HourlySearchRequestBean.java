package com.yatra.HourlySearchRequest;

public class HourlySearchRequestBean {

	private String travel_type;
	private int seats;
	private String address;
	private int city_id;
	private long pickup_time;;
	private String[] driver_criteria;
	private String[] vehicle_criteria;
	private int hourly_package_id;
	
	public int getHourly_package_id() {
		return hourly_package_id;
	}
	public void setHourly_package_id(int hourly_package_id) {
		this.hourly_package_id = hourly_package_id;
	}
	public String getTravel_type() {
		return travel_type;
	}
	public void setTravel_type(String travel_type) {
		this.travel_type = travel_type;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getCity_id() {
		return city_id;
	}
	public void setCity_id(int city_id) {
		this.city_id = city_id;
	}
	public long getPickup_time() {
		return pickup_time;
	}
	public void setPickup_time(long pickup_time) {
		this.pickup_time = pickup_time;
	}
	public String[] getDriver_criteria() {
		return driver_criteria;
	}
	public void setDriver_criteria(String[] driver_criteria) {
		this.driver_criteria = driver_criteria;
	}
	public String[] getVehicle_criteria() {
		return vehicle_criteria;
	}
	public void setVehicle_criteria(String[] vehicle_criteria) {
		this.vehicle_criteria = vehicle_criteria;
	}
	
	

}
