package com.yatra.HourlySearchRequest;

public class HourlySearchRequestObejct {
	
	public HourlySearchRequestBean hourlySearchRequestObject(String travel_type,int seats,String address,int city_id,long pickup_time,String[] driver_criteria,String[] vehicle_criteria,int hourly_package_id)
	{
		HourlySearchRequestBean hourlySearchRequestBean=new HourlySearchRequestBean();
		hourlySearchRequestBean.setTravel_type(travel_type);
		hourlySearchRequestBean.setSeats(seats);
		hourlySearchRequestBean.setAddress(address);
		hourlySearchRequestBean.setCity_id(city_id);
		hourlySearchRequestBean.setPickup_time(pickup_time);
		hourlySearchRequestBean.setDriver_criteria(driver_criteria);
		hourlySearchRequestBean.setVehicle_criteria(vehicle_criteria);
		hourlySearchRequestBean.setHourly_package_id(hourly_package_id);
		return hourlySearchRequestBean;
	}

}
