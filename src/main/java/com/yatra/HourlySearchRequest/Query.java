
package com.yatra.HourlySearchRequest;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Query {

    @SerializedName("travel_type")
    @Expose
    private String travelType;
    @SerializedName("seats")
    @Expose
    private Integer seats;
    @SerializedName("pickup")
    @Expose
    private Pickup pickup;
    @SerializedName("pickup_time")
    @Expose
    private Integer pickupTime;
    @SerializedName("driver_criteria")
    @Expose
    private List<Object> driverCriteria = null;
    @SerializedName("vehicle_criteria")
    @Expose
    private List<Object> vehicleCriteria = null;
    @SerializedName("hourly_package_id")
    @Expose
    private Integer hourlyPackageId;

    public String getTravelType() {
        return travelType;
    }

    public void setTravelType(String travelType) {
        this.travelType = travelType;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Pickup getPickup() {
        return pickup;
    }

    public void setPickup(Pickup pickup) {
        this.pickup = pickup;
    }

    public Integer getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Integer pickupTime) {
        this.pickupTime = pickupTime;
    }

    public List<Object> getDriverCriteria() {
        return driverCriteria;
    }

    public void setDriverCriteria(List<Object> driverCriteria) {
        this.driverCriteria = driverCriteria;
    }

    public List<Object> getVehicleCriteria() {
        return vehicleCriteria;
    }

    public void setVehicleCriteria(List<Object> vehicleCriteria) {
        this.vehicleCriteria = vehicleCriteria;
    }

    public Integer getHourlyPackageId() {
        return hourlyPackageId;
    }

    public void setHourlyPackageId(Integer hourlyPackageId) {
        this.hourlyPackageId = hourlyPackageId;
    }

}
