package com.yatra.testDataGenerator;

//-----------------------------------------------------------------------------------------------------------
//Description    :   Data Source helper file for testdata
//Creator        :   Alok Ranjan
//Create         :   
//Modified on/By :   -
//-----------------------------------------------------------------------------------------------------------

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.yatra.CommonUtils.CommonFunctions;
import com.yatra.CommonUtils.ReadProperties;



@SuppressWarnings({"rawtypes","unused","deprecation"})
public class TestDataGenerator {
	// private static int noOfRow; us

	/**
	 * @param args
	 */
	// private static final Logger LOGGER =
	// Logger.getLogger(DataSourceHelper.class);
	public static Object[] temp;
	public int size = 0;
	List<HSSFCell> sheetHeader = null;
	List<HSSFCell> sheetHeaderOO = null;

	public Object[][] getArray(List<List<HSSFCell>> sheetData, String tcid) {
		List<List<HSSFCell>> list = new ArrayList<List<HSSFCell>>();
		/*
		 * Iterator<HSSFCell> it = sheetHeader.iterator(); HSSFCell itVal =
		 * null;
		 */
		int indTCID = getIndexFromSheetHeaderList("TCID");
		int indYN = getIndexFromSheetHeaderList("Yes/No");
		/*
		 * while(it.hasNext()){ itVal = it.next();
		 * if(itVal.toString().trim().equals("TCID")){ indTCID =
		 * sheetHeader.indexOf((Object)itVal); } else if
		 * (itVal.toString().trim().equals("Yes/No")){ indYN =
		 * sheetHeader.indexOf((Object)itVal); } }
		 */
		Iterator<List<HSSFCell>> checkData = sheetData.iterator();
		List<HSSFCell> oneDList = null;
		while (checkData.hasNext()) {
			oneDList = (List<HSSFCell>) checkData.next();
			if (oneDList.get(indTCID).toString().equals(tcid)
					&& oneDList.get(indYN).toString().equals("Yes"))
				list.add(oneDList);
		}

		Object[][] result = new Object[list.size()][];
		try {
			for (int i = 0; i < result.length; i++) {
				result[i] = new Object[] { list.get(i) };
			}
		} catch (Exception ex) {
			System.out.println("Exception is :: " + ex);
		}
		return result; // new Object[][]{{sheetData}};
	}

	public Object[][] getArray(List<List<HSSFCell>> sheetData) {
		List<List<HSSFCell>> list = new ArrayList<List<HSSFCell>>();
		/*
		 * Iterator<HSSFCell> it = sheetHeader.iterator(); HSSFCell itVal =
		 * null;
		 */
		// int indTCID = getIndexFromSheetHeaderList("TCID");
		int indYN = getIndexFromSheetHeaderList("Yes/No");
		/*
		 * while(it.hasNext()){ itVal = it.next();
		 * if(itVal.toString().trim().equals("TCID")){ indTCID =
		 * sheetHeader.indexOf((Object)itVal); } else if
		 * (itVal.toString().trim().equals("Yes/No")){ indYN =
		 * sheetHeader.indexOf((Object)itVal); } }
		 */
		Iterator<List<HSSFCell>> checkData = sheetData.iterator();
		List<HSSFCell> oneDList = null;
		while (checkData.hasNext()) {
			oneDList = (List<HSSFCell>) checkData.next();
			if (oneDList.get(indYN).toString().equals("Yes"))
				list.add(oneDList);
		}

		Object[][] result = new Object[list.size()][];
		try {
			for (int i = 0; i < result.length; i++) {
				result[i] = new Object[] { list.get(i) };
			}
		} catch (Exception ex) {
			System.out.println("Exception is :: " + ex);
		}
		return result; // new Object[][]{{sheetData}};
	}

	public Object[][] getArrayOO(List<List<HSSFCell>> sheetData) {
		List<List<HSSFCell>> list = new ArrayList<List<HSSFCell>>();
		/*
		 * Iterator<HSSFCell> it = sheetHeader.iterator(); HSSFCell itVal =
		 * null;
		 */
		// int indTCID = getIndexFromSheetHeaderList("TCID");
		int indYN = getIndexFromSheetHeaderListOO("Yes/No");
		/*
		 * while(it.hasNext()){ itVal = it.next();
		 * if(itVal.toString().trim().equals("TCID")){ indTCID =
		 * sheetHeader.indexOf((Object)itVal); } else if
		 * (itVal.toString().trim().equals("Yes/No")){ indYN =
		 * sheetHeader.indexOf((Object)itVal); } }
		 */
		Iterator<List<HSSFCell>> checkData = sheetData.iterator();
		List<HSSFCell> oneDList = null;
		while (checkData.hasNext()) {
			oneDList = (List<HSSFCell>) checkData.next();
			if (oneDList.get(indYN).toString().equalsIgnoreCase("Yes"))
				list.add(oneDList);
		}

		Object[][] result = new Object[list.size()][];
		try {
			for (int i = 0; i < result.length; i++) {
				result[i] = new Object[] { list.get(i) };
			}
		} catch (Exception ex) {
			System.out.println("Exception is :: " + ex);
		}
		return result; // new Object[][]{{sheetData}};
	}

	public List<List<HSSFCell>> getNewArray(List<List<HSSFCell>> sheetData,
			String tcid) {
		List<List<HSSFCell>> list = new ArrayList<List<HSSFCell>>();
		/*
		 * Iterator<HSSFCell> it = sheetHeader.iterator(); HSSFCell itVal =
		 * null;
		 */
		int indTCID = getIndexFromSheetHeaderList("TCID");
		int indYN = getIndexFromSheetHeaderList("Yes/No");
		/*
		 * while(it.hasNext()){ itVal = it.next();
		 * if(itVal.toString().trim().equals("TCID")){ indTCID =
		 * sheetHeader.indexOf((Object)itVal); } else if
		 * (itVal.toString().trim().equals("Yes/No")){ indYN =
		 * sheetHeader.indexOf((Object)itVal); } }
		 */
		Iterator<List<HSSFCell>> checkData = sheetData.iterator();
		List<HSSFCell> oneDList = null;
		while (checkData.hasNext()) {
			oneDList = (List<HSSFCell>) checkData.next();
			if (oneDList.get(indTCID).toString().equals(tcid)
					&& oneDList.get(indYN).toString().equals("Yes"))
				list.add(oneDList);
		}

		// Object[][] result = new Object[list.size()][];
		// try {
		// for (int i = 0; i < result.length; i++) {
		// result[i] = new Object[] { list.get(i) };
		// }
		// } catch (Exception ex) {
		// System.out.println("Exception is :: " + ex);
		// }
		return list; // new Object[][]{{sheetData}};
	}

	public static List<List<HSSFCell>> getData(String workBook, String sheetName) {
		File dir1 = new File(".");
		List<List<HSSFCell>> sheetData = new ArrayList<List<HSSFCell>>();
		FileInputStream fis = null;
		//HSSFWorkbook wb = null;
		//HSSFSheet sheet = null;
		HSSFWorkbook wb=null;
		HSSFSheet sheet=null;
		
		try {
			String strBasePath = dir1.getCanonicalPath();
			System.out.println(strBasePath);

			String fileName = new ReadProperties().readPropertyPath(workBook);
			System.out.println("The file is :: " + fileName);

		//	File file=new File(fileName);
			fis = new FileInputStream(fileName);
			//wb = new HSSFWorkbook(fis);
			//sheet = wb.getSheet(sheetName);
			wb=new HSSFWorkbook(fis);
			sheet=wb.getSheet(sheetName);
			
			
			
//			Workbook workbook=WorkbookFactory.create(file);
	//		Sheet sheet=workbook.getSheet(sheetName);

			// Iterator<HSSFSheet> sheetItr = wb.iterator();
			System.out.println(sheet.getPhysicalNumberOfRows());

			Iterator<Row> rows = sheet.rowIterator();

			while (rows.hasNext()) {
				HSSFRow rown = (HSSFRow) rows.next();
				System.out
						.println("Cell :: " + rown.getPhysicalNumberOfCells());
				Iterator<Cell> cells = rown.cellIterator();

				List<HSSFCell> data = new ArrayList<HSSFCell>();
				while (cells.hasNext()) {
					HSSFCell celln = (HSSFCell) cells.next();
					// System.out.println("Cell Type is :: " +
					// celln.getCellType());

					celln = (HSSFCell) castCellType(celln);
					data.add(celln);
				}
				sheetData.add(data);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// showExelData(sheetData);
		return sheetData;
	}

	public static List<List<HSSFCell>> getDataForOldExcel(String workBook,
			String sheetName) {
		File dir1 = new File(".");
		List<List<HSSFCell>> sheetData = new ArrayList<List<HSSFCell>>();
		FileInputStream fis = null;
		HSSFWorkbook wb = null;
		HSSFSheet sheet = null;

		try {
			String strBasePath = dir1.getCanonicalPath();
//			System.out.println(strBasePath);

			new com.yatra.CommonUtils.ReadProperties();
			String file = ReadProperties.readPropertyPath(workBook);
			System.out.println("The file is :: " + file);

			fis = new FileInputStream(file);
			wb = new HSSFWorkbook(fis);
			sheet = wb.getSheet(sheetName);

			// Iterator<HSSFSheet> sheetItr = wb.iterator();
//			System.out.println(sheet.getPhysicalNumberOfRows());

			Iterator<Row> rows = sheet.rowIterator();

			while (rows.hasNext()) {
				HSSFRow rown = (HSSFRow) rows.next();
//				System.out.println("Cell :: " + rown.getPhysicalNumberOfCells());
				Iterator<Cell> cells = rown.cellIterator();

				List<HSSFCell> data = new ArrayList<HSSFCell>();
				while (cells.hasNext()) {
					HSSFCell celln = (HSSFCell) cells.next();
					// System.out.println("Cell Type is :: " +
					// celln.getCellType());

					celln = (HSSFCell) castCellType(celln);
					data.add(celln);
				}
				sheetData.add(data);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// showExelData(sheetData);
		return sheetData;
	}

	public static void showExelData(List<List<HSSFCell>> sheetData) {
		//
		// Iterates the data and print it out to the console.
		//
		for (int i = 0; i < sheetData.size(); i++) {
			List<HSSFCell> list = (List<HSSFCell>) sheetData.get(i);
			for (int j = 0; j < list.size(); j++) {
				Cell cell = (Cell) list.get(j);
				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
					System.out.print(cell.getNumericCellValue());
				} else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					System.out.print(cell.getRichStringCellValue());
				} else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
					System.out.print(cell.getBooleanCellValue());
				}
				if (j < list.size() - 1) {
					System.out.print(", ");
				}
			}
			System.out.println("");
		}
	}

	public static Cell castCellType(Cell cell) {
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			if (DateUtil.isCellDateFormatted(cell)) {
				System.out.print(cell.getDateCellValue());
				cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			} else
				cell.setCellType(Cell.CELL_TYPE_STRING);
		} else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			cell.getRichStringCellValue();
		} else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
		} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
		} else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
		} else if (cell.getCellType() == Cell.CELL_TYPE_ERROR) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
		}
		return cell;
	}

	public static String convertHSSFCellToString(HSSFCell cell) {
		String cellValue = null;
		if (cell != null) {
			cellValue = cell.toString();
			cellValue = cellValue.trim();
		} else {
			cellValue = "";
		}
		return cellValue;
	}

	public Object[][] getTestDataForQuickBook(String tcid, String sSheetName)
			throws Exception {

		Object[][] array = null;

		try {
			System.out.println("getTestDataForQuickBook : Start");

			String sWorkbookName = "SanityTestData";
			// To Do - Make constants for sheet name in Constants.java
			List<List<HSSFCell>> listData = getData(sWorkbookName, sSheetName);
			sheetHeader = listData.get(0);
			listData.remove(0);

			array = getArray(listData, tcid);
			size = array.length;
			System.out.println(size);

		} catch (Exception e) {
			System.out.println(
					"Exception in getTestDataForQuickBook: ");
		} finally {
			System.out.println("getTestDataForQuickBook : End");
		}
		return array;

	}

	public Object[][] getTestDataForSanityTest(String tcid, String sSheetName)
			throws Exception {

		Object[][] array = null;

		try {
			System.out.println("getTestDataForSanityTest : Start");

			String sWorkbookName = "SanityTestData";
			// To Do - Make constants for sheet name in Constants.java
			List<List<HSSFCell>> listData = getData(sWorkbookName, sSheetName);
			sheetHeader = listData.get(0);
			listData.remove(0);

			array = getArray(listData, tcid);
			size = array.length;
			System.out.println(size);

		} catch (Exception e) {
			System.out.println("Exception in getTestDataForSanityTest: ");
		} finally {
			System.out.println("getTestDataForSanityTest : End");
		}
		return array;

	}

	public Object[][] getTestDataForSanityTest(String sSheetName)
			throws Exception {

		Object[][] array = null;

		try {
			System.out.println("getTestDataForSanityTest : Start");

			String sWorkbookName = "SanityTestData";
			// To Do - Make constants for sheet name in Constants.java
			List<List<HSSFCell>> listData = getData(sWorkbookName, sSheetName);
			System.out.println("get data called 1");
			sheetHeader = listData.get(0);
			listData.remove(0);

			array = getArray(listData);
			size = array.length;
			System.out.println(size);

		} catch (Exception e) {
			System.out.println("Exception in getTestDataForSanityTest: "+e);
		} finally {
			System.out.println("getTestDataForSanityTest : End");
		}
		return array;

	}

	public List<List<HSSFCell>> getTestDataForSanityTests(String tcid,
			String sSheetName) throws Exception {

		List<List<HSSFCell>> array = null;

		try {
			System.out.println("getTestDataForSanityTest : Start");

			String sWorkbookName = "SanityTestData";
			// To Do - Make constants for sheet name in Constants.java
			List<List<HSSFCell>> listData = getData(sWorkbookName, sSheetName);
			sheetHeader = listData.get(0);
			listData.remove(0);

			array = getNewArray(listData, tcid);
			size = array.size();
			System.out.println(size);

		} catch (Exception e) {
			System.out.println("Exception in getTestDataForSanityTest: " );
		} finally {
			System.out.println("getTestDataForSanityTest : End");
		}
		return array;

	}

	public Object[][] getXMLTestDataForSanityTest(String tcid, String sSheetName)
			throws Exception {

		Object[][] array = null;

		try {
			System.out.println("getTestDataForSanityTest : Start");

			String sWorkbookName = "SanityTestData";
			// To Do - Make constants for sheet name in Constants.java
			List<List<HSSFCell>> listData = getData(sWorkbookName, sSheetName);
			sheetHeader = listData.get(0);
			listData.remove(0);

			array = getArray(listData, tcid);
			size = array.length;
			System.out.println(size);

		} catch (Exception e) {
			System.out.println("Exception in getTestDataForSanityTest: ");
		} finally {
			System.out.println("getTestDataForSanityTest : End");
		}
		return array;

	}

	public Object[][] getOOTestDataForSanityTest(String sSheetName)
			throws Exception {

		Object[][] array = null;

		try {
			System.out.println("getTestDataForSanityTest : Start");

			String sWorkbookName = "SanityTestDataOO";
			// To Do - Make constants for sheet name in Constants.java
			List<List<HSSFCell>> listData = getDataForOldExcel(sWorkbookName,
					sSheetName);
			sheetHeaderOO = listData.get(0);
			listData.remove(0);

			array = getArrayOO(listData);
			size = array.length;
			System.out.println("________________________________");
			System.out.println("\nTotal test to Run(YES): "+size);
			System.out.println("________________________________");
			System.out.println();

		} catch (Exception e) {
			System.out.println("Exception in getTestDataForSanityTest: ");
		} finally {
			System.out.println("getTestDataForSanityTest : End");
		}
		return array;

	}

	public Object[][] getXMLTestDataForSanityTest(String sSheetName,
			Boolean ISEXCEL) throws Exception {

		Object[][] array = null;

		try {
//			MyPropsLogger.getLogger().info("getTestDataForSanityTest : Start");

			String sWorkbookName = "SanityTestData";
			// To Do - Make constants for sheet name in Constants.java
			List<List<HSSFCell>> listData = getData(sWorkbookName, sSheetName);
			sheetHeader = listData.get(0);
			listData.remove(0);

			array = getArray(listData);
			size = array.length;
			System.out.println(size);

		} catch (Exception e) {
//			MyPropsLogger.getLogger().error(
//					"Exception in getTestDataForSanityTest: " + e.getMessage(),
//					e);
		} finally {
//			MyPropsLogger.getLogger().info("getTestDataForSanityTest : End");
		}
		return array;

	}

	public int getIndexFromSheetHeaderList(String hdrColName) {
		Iterator<HSSFCell> hdrItr = sheetHeader.iterator();
		HSSFCell hdrVal = null;
		int indOfCol = 0;
		while (hdrItr.hasNext()) {
			hdrVal = hdrItr.next();
			if (hdrVal.toString().trim().equals(hdrColName))
				indOfCol = sheetHeader.indexOf((Object) hdrVal);
		}
		return indOfCol;

	}

	public int getIndexFromSheetHeaderListOO(String hdrColName) {
		Iterator<HSSFCell> hdrItr = sheetHeaderOO.iterator();
		HSSFCell hdrVal = null;
		int indOfCol = 0;
		while (hdrItr.hasNext()) {
			hdrVal = hdrItr.next();
			if (hdrVal.toString().trim().equals(hdrColName))
				indOfCol = sheetHeaderOO.indexOf((Object) hdrVal);
		}
		return indOfCol;

	}

	/*public Object[][] getTestDataForSanityTestXML(String sheetname) {
		// TODO Auto-generated method stub
		Object[][] array = null;

		try {
			MyPropsLogger.getLogger().info("getTestDataForSanityTest : Start");

			// List<List<HSSFCell>> listData = getData(sWorkbookName,
			// sheetname);
			// sheetHeader = listData.get(0);
			// listData.remove(0);

			// array = getArray(listData);
			// size = array.length;
			// System.out.println(size);

		} catch (Exception e) {
			MyPropsLogger.getLogger().error(
					"Exception in getTestDataForSanityTest: " + e.getMessage(),
					e);
		} finally {
			MyPropsLogger.getLogger().info("getTestDataForSanityTest : End");
		}
		return array;
	}*/

/*
	public void createSummeryReport(String workBook, String sheetName,
			 List testDataArray, String sBroswer, String result) {
		File dir1 = new File(".");
		List<List<HSSFCell>> sheetData = new ArrayList<List<HSSFCell>>();
		FileInputStream fis = null;
		HSSFWorkbook wb = null;
		HSSFSheet sheet = null;
		String testData = null;
		String reportData = null;
		int count = 0;
		String resultPass = "0";
		String resultfail = "0";
		int intData = 0;
		int intPass = 0;
		int intFail = 0;
		int intTotalCaseExecuted = 0;
		int intPassPrecentage = 0;
		try {
			String strBasePath = dir1.getCanonicalPath();
			System.out.println(strBasePath);

			String file = new ReadProperties().readProperty(workBook);
			System.out.println("The file is :: " + file);

			File f = new File(file);

			if (f.exists()) {

				if (sheetName.equalsIgnoreCase("Holidays_sanityscenariots")) {
					testData = testDataArray.get(0).toString() + sBroswer;

					System.out.println(testData);
				} else {
					testData = testDataArray.get(0).toString()
							+ sBroswer
							+ testDataArray.get(Constants.TRAVEL_TYPE)
									.toString()
							+ testDataArray.get(Constants.TRIP_TYPE).toString();
					
					System.out.println(testData);
				}
				fis = new FileInputStream(file);
				wb = new HSSFWorkbook(fis);
				sheet = wb.getSheet(sheetName);
				if (sheet != null) {

					// Iterator<HSSFSheet> sheetItr = wb.iterator();
					System.out.println(sheet.getPhysicalNumberOfRows());

					Iterator<Row> rows = sheet.rowIterator();
					// Iterate all the rows
					while (rows.hasNext()) {
						HSSFRow rown = (HSSFRow) rows.next();
						System.out.println(rown.getRowNum());
						System.out.println("Cell :: "
								+ rown.getPhysicalNumberOfCells());
						// Add record when number of cell is 7 in a row
						if (rown.getPhysicalNumberOfCells() == 7) {
							// Iterate all the column in a row
							Iterator<Cell> cells = rown.cellIterator();

							List<HSSFCell> data = new ArrayList<HSSFCell>();
							while (cells.hasNext()) {
								HSSFCell celln = (HSSFCell) cells.next();
								// System.out.println("Cell Type is :: " +
								// celln.getCellType());

								celln = (HSSFCell) castCellType(celln);
								data.add(celln);
							}
							// Add cell data in to the list
							sheetData.add(data);

							reportData = (String) sheetData
									.get(rown.getRowNum()).get(0).toString()
									+ (String) sheetData.get(rown.getRowNum())
											.get(2).toString()
									+ (String) sheetData.get(rown.getRowNum())
											.get(3).toString()
									+ (String) sheetData.get(rown.getRowNum())
											.get(4).toString();
							System.out.println(reportData);

							// compare cell data and test case data
							if (!reportData.equalsIgnoreCase(testData)) {

							} else {
								count = count + 1;
								if (result.equalsIgnoreCase("True")) {
									Cell cell = null;
									cell = sheet.getRow(rown.getRowNum())
											.getCell(5);
									intData = Integer.parseInt(cell
											.getStringCellValue()) + 1;

									cell.setCellValue(String.valueOf(intData));
								} else {
									Cell cell = null;
									cell = sheet.getRow(rown.getRowNum())
											.getCell(6);
									intData = Integer.parseInt(cell
											.getStringCellValue()) + 1;

									cell.setCellValue(String.valueOf(intData));
								}
							}

						}

					}

					if (count == 0) {

						if (result.equalsIgnoreCase("True")) {
							resultPass = "1";
						} else {
							resultfail = "1";
						}
						// create new row.

						// index from 0,0... cell A1 is cell(0,0)
						HSSFRow row1 = sheet.createRow((short) sheet
								.getPhysicalNumberOfRows());

						for (int i = 0; i <= 6; i++) {
							String sCelldata = null;
							if (i == 0) {
								sCelldata = testDataArray.get(Constants.TCID)
										.toString();
							} else if (i == 1) {

								sCelldata = testDataArray.get(
										Constants.DESCRIPTION).toString();

							} else if (i == 2) {

								sCelldata = sBroswer;

							} else if (i == 3) {

								sCelldata = testDataArray.get(
										Constants.TRAVEL_TYPE).toString();

							} else if (i == 4) {

								sCelldata = testDataArray.get(
										Constants.TRIP_TYPE).toString();

							} else if (i == 5) {
								sCelldata = resultPass;

							} else if (i == 6) {

								sCelldata = resultfail;
							}

							HSSFCell cellA1 = row1.createCell((short) i);
							cellA1.setCellValue(sCelldata);
							HSSFCellStyle cellStyle = wb.createCellStyle();
							cellStyle
									.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
							cellStyle
									.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
							cellA1.setCellStyle(cellStyle);
							sheet.autoSizeColumn(i);

						}

					}

					// Update Header Data

					sheet = wb.getSheet("Summary Report");
					Cell cell = null;
					cell = sheet.getRow(8).getCell(4);
					intTotalCaseExecuted = Integer.parseInt(cell
							.getStringCellValue()) + 1;

					cell.setCellValue(String.valueOf(intTotalCaseExecuted));

					if (result.equalsIgnoreCase("True")) {
						cell = sheet.getRow(9).getCell(4);
						intPass = Integer.parseInt(cell.getStringCellValue()) + 1;

						cell.setCellValue(String.valueOf(intPass));
					} else {
						cell = sheet.getRow(10).getCell(4);
						intFail = Integer.parseInt(cell.getStringCellValue()) + 1;

						cell.setCellValue(String.valueOf(intFail));
					}

					 intPassPrecentage = (int)((Double.parseDouble(sheet.getRow(9).getCell(4).getStringCellValue())) /
							 (Double.parseDouble(sheet.getRow(8).getCell(4).getStringCellValue()))*100);

					cell = sheet.getRow(11).getCell(4);
					System.out.println(intPassPrecentage);
					cell.setCellValue(String.valueOf(intPassPrecentage));

					fis.close();

					FileOutputStream outFile = new FileOutputStream(new File(
							file));
					wb.write(outFile);
					outFile.close();
				} else {
					createXlsFile(workBook, sheetName);
				}
			}

			else {

				// create excel file when not exit
				createXlsFile(workBook, sheetName);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// showExelData(sheetData);

	}*/

	public void createXlsFile(String workBook, String sheetName) {

		FileOutputStream fileOut;
		HSSFWorkbook workbook;
		HSSFSheet worksheet;
		HSSFRow row1;
		HSSFCell cell;

		try {
			workbook = new HSSFWorkbook();

			String filename = new ReadProperties().readPropertyPath(workBook);
			System.out.println("The file is :: " + filename);

			File file = new File(filename);

			if (!file.exists()) {
				worksheet = workbook.createSheet("Summary Report");
				HSSFCellStyle cellStyle = workbook.createCellStyle();
				cellStyle.setFillForegroundColor(HSSFColor.WHITE.index);
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				cellStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
				cellStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
				cellStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
				cellStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
				Font font = workbook.createFont();

				// index from 0,0... cell A1 is cell(0,0)
				row1 = worksheet.createRow((short) 0);
				row1 = worksheet.createRow((short) 1);
				row1 = worksheet.createRow((short) 2);
				row1 = worksheet.createRow((short) 3);
				row1 = worksheet.createRow((short) 4);
				row1 = worksheet.createRow((short) 5);
				row1 = worksheet.createRow((short) 6);

				row1 = worksheet.createRow((short) 7);
				cell = row1.createCell((short) 3);
				cell.setCellValue(" High Level Report Summary ");

				cellStyle.setFont(font);
				cell.setCellStyle(cellStyle);
				worksheet.autoSizeColumn(3);
				// worksheet.addMergedRegion(CellRangeAddress.valueOf("D8:F8"));

				row1 = worksheet.createRow((short) 8);

				cell = row1.createCell((short) 3);
				cell.setCellValue("Total no. of TestCases executed   :");

				cell.setCellStyle(cellStyle);
				worksheet.autoSizeColumn(3);
				// worksheet.addMergedRegion(CellRangeAddress.valueOf("A2:C2"));

				cell = row1.createCell((short) 4);
				cell.setCellValue("0");

				cell.setCellStyle(cellStyle);

				// worksheet.addMergedRegion(new
				// org.apache.poi.ss.util.CellRangeAddress(1, 1, 3, 5));

				row1 = worksheet.createRow((short) 9);

				cell = row1.createCell((short) 3);
				cell.setCellValue(" Total no. of TestCases Passed  : ");

				cell.setCellStyle(cellStyle);
				worksheet.autoSizeColumn(3);
				// worksheet.addMergedRegion(CellRangeAddress.valueOf("A3:C3"));
				cell = row1.createCell((short) 4);
				cell.setCellValue("0");

				cell.setCellStyle(cellStyle);
				worksheet.autoSizeColumn(4);
				// worksheet.addMergedRegion(CellRangeAddress.valueOf("D3:F3"));

				row1 = worksheet.createRow((short) 10);

				cell = row1.createCell((short) 3);
				cell.setCellValue(" Total no. of TestCases Failed : ");

				font.setColor(HSSFColor.BLACK.index);
				cellStyle.setFont(font);
				cell.setCellStyle(cellStyle);
				worksheet.autoSizeColumn(3);
				// worksheet.addMergedRegion(CellRangeAddress.valueOf("A4:C4"));
				cell = row1.createCell((short) 4);
				cell.setCellValue("0");

				font.setColor(HSSFColor.BLUE.index);
				cellStyle.setFont(font);
				cell.setCellStyle(cellStyle);
				worksheet.autoSizeColumn(4);
				// worksheet.addMergedRegion(CellRangeAddress.valueOf("D4:F4"));
				row1 = worksheet.createRow((short) 11);

				cell = row1.createCell((short) 3);
				cell.setCellValue(" Total % of TestCases Passed : ");

				cell.setCellStyle(cellStyle);
				worksheet.autoSizeColumn(3);

				// worksheet.addMergedRegion(CellRangeAddress.valueOf("A5:C5"));
				cell = row1.createCell((short) 4);
				cell.setCellValue("0");

				cell.setCellStyle(cellStyle);
				worksheet.autoSizeColumn(4);
				// worksheet.addMergedRegion(CellRangeAddress.valueOf("D5:F5"));

			} else {

				HSSFWorkbook myWorkBook;
				try {
					myWorkBook = (HSSFWorkbook) WorkbookFactory.create(file);
					workbook = myWorkBook;
				} catch (InvalidFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				HSSFSheet mySheet = (HSSFSheet) workbook.createSheet(sheetName);
				if (!sheetName.equalsIgnoreCase("Holidays_sanityscenariots")) {

					row1 = mySheet.createRow((short) 0);

					HSSFCellStyle cellStyle = workbook.createCellStyle();
					for (int i = 0; i <= 6; i++) {
						String strData = null;
						if (i == 0) {
							strData = " Test Case  ";
						} else if (i == 1) {
							strData = " Test Case Description  ";
						} else if (i == 2) {
							strData = "  Browser  ";

						} else if (i == 3) {
							strData = "  Travel Type  ";

						} else if (i == 4) {
							strData = "  Trip Type  ";
						} else if (i == 5) {
							strData = "  Pass  ";
						} else if (i == 6) {
							strData = "  Fail  ";
						}
						HSSFCell cellA1 = row1.createCell((short) i);
						cellA1.setCellValue(strData);
						cellStyle = workbook.createCellStyle();
						cellStyle
								.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
						cellStyle
								.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
						cellA1.setCellStyle(cellStyle);
						mySheet.autoSizeColumn(i);

					}
				} else {
					row1 = mySheet.createRow((short) 0);

					HSSFCellStyle cellStyle = workbook.createCellStyle();
					for (int i = 0; i <= 4; i++) {
						String strData = null;
						if (i == 0) {
							strData = " Test Case  ";
						} else if (i == 1) {
							strData = " Test Case Description  ";
						} else if (i == 2) {
							strData = "  Browser  ";

						} else if (i == 3) {
							strData = "  Pass  ";

						} else if (i == 4) {
							strData = "  Fail  ";
						}
						HSSFCell cellA1 = row1.createCell((short) i);
						cellA1.setCellValue(strData);
						cellStyle = workbook.createCellStyle();
						cellStyle
								.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
						cellStyle
								.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
						cellA1.setCellStyle(cellStyle);
						mySheet.autoSizeColumn(i);

					}
				}
			}

			fileOut = new FileOutputStream(workBook + ".xls");

			workbook.write(fileOut);
			fileOut.flush();
			fileOut.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public void createSummeryReportTaxiService(String workBook,
			String sheetName, List testDataArray, String sCityName) {
		File dir1 = new File(".");
		
		FileInputStream fis = null;
		HSSFWorkbook wb = null;
		HSSFSheet sheet = null;
		
		int i = 0;
		int size = testDataArray.size();
		try {
			String strBasePath = dir1.getCanonicalPath();
			System.out.println(strBasePath);

			new ReadProperties();
			String file = ReadProperties.readPropertyPath(workBook);
			System.out.println("The file is :: " + file);

			File f = new File(file);

			if (f.exists()) {

				fis = new FileInputStream(file);
				wb = new HSSFWorkbook(fis);
				sheet = wb.getSheet(sheetName);
				// create new row.
				// index from 0,0... cell A1 is cell(0,0)

				for (i = 0; i < size; i++) {
					HSSFRow row1 = sheet.createRow((short) sheet
							.getPhysicalNumberOfRows());

					HSSFCell cellA1 = row1.createCell((short) 0);
					cellA1.setCellValue(testDataArray.get(i).toString());
					HSSFCellStyle cellStyle = wb.createCellStyle();
					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					cellA1.setCellStyle(cellStyle);
					sheet.autoSizeColumn(0);

					HSSFCell cellA2 = row1.createCell((short) 1);
					cellA1.setCellValue(sCityName);

					cellStyle
							.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
					cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					cellA1.setCellStyle(cellStyle);
					sheet.autoSizeColumn(1);

				}

				fis.close();

				FileOutputStream outFile = new FileOutputStream(new File(file));
				wb.write(outFile);
				outFile.close();

			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// showExelData(sheetData);

	}
}
