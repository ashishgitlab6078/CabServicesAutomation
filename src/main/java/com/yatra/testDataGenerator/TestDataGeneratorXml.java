package com.yatra.testDataGenerator;

/**@author - vinod.kumar
*/

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class TestDataGeneratorXml {

	private static int indexOfRunTest = 0;

	public static Object[] temp;
	public int size = 0;

	public Object[][] getTestDataFromXML(String xmlPath) {
		Object[][] result = null;
		Object[][] array = null;

		try {

			Thread.sleep(1000);
			List<List<String>> listData = getData(xmlPath);
			array = getArrayxml(listData);
			
			System.out.println("\nTotal Tests: "+array.length);

//			result = new Object[listData.size()][];
//			for (int i = 0; i < result.length; i++) {
//				result[i] = new Object[] { listData.get(i) };
//			}

		} catch (Exception e) {

		}

		return array;
	}

	public static Object[][] getArrayxml(List<List<String>> listData) {

		List<List<String>> list = new ArrayList<List<String>>();

		Iterator<List<String>> checkData = listData.iterator();
		List<String> oneDList = null;

		while (checkData.hasNext()) {
			oneDList = checkData.next();

			String temp = oneDList.get(indexOfRunTest).toString();

			if (temp.equalsIgnoreCase("YES"))
				list.add(oneDList);
		}

		Object[][] result = new Object[list.size()][];
		try {
			for (int i = 0; i < result.length; i++) {
				result[i] = new Object[] { list.get(i) };
			}

		} catch (Exception ex) {
			System.out.println("Exception is :: " + ex);
		}

//		System.out.println(result);

		return result;
		// new Object[][]{{sheetData}};
	}

	public static List<List<String>> getData(String xmlPath) {

		List<List<String>> nodedata = new ArrayList<List<String>>();

		try {

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new File(xmlPath));

			// normalize text representation
			doc.getDocumentElement().normalize();
			// System.out.println("Root element of the doc is " +
			// doc.getDocumentElement().getNodeName());

			NodeList testID = doc.getElementsByTagName("test");
			int totalcase = testID.getLength();
			// System.out.println("Total test Cases : " + totalcase);

			for (int s = 0; s < totalcase; s++) {

				List<String> finaldata = new ArrayList<String>();

				Node firstPersonNode = testID.item(s);
				if (firstPersonNode.getNodeType() == Node.ELEMENT_NODE) {

					Element firstPersonElement = (Element) firstPersonNode;
					finaldata.add(firstPersonElement.getAttribute("id").trim());

					// -------
					String Description = firstPersonElement.getAttribute("description");
					finaldata.add(Description.trim());

					// ----
					NodeList domain = firstPersonElement.getElementsByTagName("domain");
					Element Elem = (Element) domain.item(0);
					NodeList nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					// ----
					NodeList origin = firstPersonElement.getElementsByTagName("origin");
					Elem = (Element) origin.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					// ----
					NodeList destination = firstPersonElement.getElementsByTagName("destination");
					Elem = (Element) destination.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					// -----
					NodeList departdate = firstPersonElement.getElementsByTagName("departdate");
					Elem = (Element) departdate.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					// -----
					NodeList returndate = firstPersonElement.getElementsByTagName("returndate");
					Elem = (Element) returndate.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					// ----
					NodeList tripType = firstPersonElement.getElementsByTagName("tripType");
					Elem = (Element) tripType.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());
					// ---

					NodeList noOfAdults = firstPersonElement.getElementsByTagName("noOfAdults");
					Elem = (Element) noOfAdults.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					// ---
					NodeList noOfChildren = firstPersonElement.getElementsByTagName("noOfChildren");
					Elem = (Element) noOfChildren.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());
					// ---

					NodeList noOfInfants = firstPersonElement.getElementsByTagName("noOfInfants");
					Elem = (Element) noOfInfants.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());
					// ---

					NodeList travelClass = firstPersonElement.getElementsByTagName("travelClass");
					Elem = (Element) travelClass.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());
					// ---

					NodeList osVersion = firstPersonElement.getElementsByTagName("osVersion");
					Elem = (Element) osVersion.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());
					// ---

					NodeList appVersion = firstPersonElement.getElementsByTagName("appVersion");
					Elem = (Element) appVersion.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					NodeList promoCode = firstPersonElement.getElementsByTagName("promoCode");
					Elem = (Element) promoCode.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					NodeList insurance = firstPersonElement.getElementsByTagName("insurance");
					Elem = (Element) insurance.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());
					
					NodeList login = firstPersonElement.getElementsByTagName("login");
					Elem = (Element) login.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());
					
					// -------runtest should be in the end------//

					NodeList runtest = firstPersonElement.getElementsByTagName("runTest");
					Elem = (Element) runtest.item(0);
					nodelist = Elem.getChildNodes();
					finaldata.add(nodelist.item(0).getNodeValue().trim());

					indexOfRunTest = finaldata.size() - 1;

				} // end of if clause

				nodedata.add(finaldata);
			}

			System.out.println(nodedata);

		} catch (SAXParseException err) {
			System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
			System.out.println(" " + err.getMessage());

		} catch (SAXException e) {
			Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();

		} catch (Throwable t) {
			t.printStackTrace();
		}
		return nodedata;

	}

}
