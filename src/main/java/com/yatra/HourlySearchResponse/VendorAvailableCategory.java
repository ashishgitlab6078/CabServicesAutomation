
package com.yatra.HourlySearchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorAvailableCategory {

    @SerializedName("fare_details")
    @Expose
    private FareDetails fareDetails;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("fare_id")
    @Expose
    private String fareId;
    @SerializedName("yatra_category_id")
    @Expose
    private String yatraCategoryId;
    @SerializedName("vendor_category")
    @Expose
    private VendorCategory vendorCategory;

    public FareDetails getFareDetails() {
        return fareDetails;
    }

    public void setFareDetails(FareDetails fareDetails) {
        this.fareDetails = fareDetails;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getFareId() {
        return fareId;
    }

    public void setFareId(String fareId) {
        this.fareId = fareId;
    }

    public String getYatraCategoryId() {
        return yatraCategoryId;
    }

    public void setYatraCategoryId(String yatraCategoryId) {
        this.yatraCategoryId = yatraCategoryId;
    }

    public VendorCategory getVendorCategory() {
        return vendorCategory;
    }

    public void setVendorCategory(VendorCategory vendorCategory) {
        this.vendorCategory = vendorCategory;
    }

}
