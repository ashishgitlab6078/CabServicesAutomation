
package com.yatra.HourlySearchResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HourlySearchResponse {

    @SerializedName("travel_type")
    @Expose
    private String travelType;
    @SerializedName("vendors")
    @Expose
    private List<Vendor> vendors = null;
    @SerializedName("yatra_terms_url")
    @Expose
    private String yatraTermsUrl;
    @SerializedName("search_id")
    @Expose
    private String searchId;
    @SerializedName("created_at")
    @Expose
    private long createdAt;
    @SerializedName("yatra_categories")
    @Expose
    private List<YatraCategory> yatraCategories = null;
    @SerializedName("vendor_available_categories")
    @Expose
    private List<VendorAvailableCategory> vendorAvailableCategories = null;

    public String getTravelType() {
        return travelType;
    }

    public void setTravelType(String travelType) {
        this.travelType = travelType;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public String getYatraTermsUrl() {
        return yatraTermsUrl;
    }

    public void setYatraTermsUrl(String yatraTermsUrl) {
        this.yatraTermsUrl = yatraTermsUrl;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public List<YatraCategory> getYatraCategories() {
        return yatraCategories;
    }

    public void setYatraCategories(List<YatraCategory> yatraCategories) {
        this.yatraCategories = yatraCategories;
    }

    public List<VendorAvailableCategory> getVendorAvailableCategories() {
        return vendorAvailableCategories;
    }

    public void setVendorAvailableCategories(List<VendorAvailableCategory> vendorAvailableCategories) {
        this.vendorAvailableCategories = vendorAvailableCategories;
    }

}
