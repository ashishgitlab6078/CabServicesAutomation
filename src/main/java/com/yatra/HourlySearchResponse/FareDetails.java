
package com.yatra.HourlySearchResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FareDetails {

    @SerializedName("components")
    @Expose
    private List<Object> components = null;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("ecash")
    @Expose
    private Object ecash;

    public List<Object> getComponents() {
        return components;
    }

    public void setComponents(List<Object> components) {
        this.components = components;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Object getEcash() {
        return ecash;
    }

    public void setEcash(Object ecash) {
        this.ecash = ecash;
    }

}
