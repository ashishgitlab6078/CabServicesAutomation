
package com.yatra.HourlySearchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vendor {

    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("logo_url")
    @Expose
    private String logoUrl;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("external_id")
    @Expose
    private String externalId;
    @SerializedName("refund_policy_url")
    @Expose
    private String refundPolicyUrl;
    @SerializedName("terms_url")
    @Expose
    private String termsUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getRefundPolicyUrl() {
        return refundPolicyUrl;
    }

    public void setRefundPolicyUrl(String refundPolicyUrl) {
        this.refundPolicyUrl = refundPolicyUrl;
    }

    public String getTermsUrl() {
        return termsUrl;
    }

    public void setTermsUrl(String termsUrl) {
        this.termsUrl = termsUrl;
    }

}
