
package com.yatra.HourlySearchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ecash {

    @SerializedName("ecash_earn")
    @Expose
    private EcashEarn ecashEarn;
    @SerializedName("ecash_redeem")
    @Expose
    private EcashRedeem ecashRedeem;

    public EcashEarn getEcashEarn() {
        return ecashEarn;
    }

    public void setEcashEarn(EcashEarn ecashEarn) {
        this.ecashEarn = ecashEarn;
    }

    public EcashRedeem getEcashRedeem() {
        return ecashRedeem;
    }

    public void setEcashRedeem(EcashRedeem ecashRedeem) {
        this.ecashRedeem = ecashRedeem;
    }

}
