
package com.yatra.HourlySearchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Versions {

    @SerializedName("xhdpi")
    @Expose
    private String xhdpi;
    @SerializedName("hdpi")
    @Expose
    private String hdpi;
    @SerializedName("xxhdpi")
    @Expose
    private String xxhdpi;
    @SerializedName("xxxhdpi")
    @Expose
    private String xxxhdpi;
    @SerializedName("mdpi")
    @Expose
    private String mdpi;

    public String getXhdpi() {
        return xhdpi;
    }

    public void setXhdpi(String xhdpi) {
        this.xhdpi = xhdpi;
    }

    public String getHdpi() {
        return hdpi;
    }

    public void setHdpi(String hdpi) {
        this.hdpi = hdpi;
    }

    public String getXxhdpi() {
        return xxhdpi;
    }

    public void setXxhdpi(String xxhdpi) {
        this.xxhdpi = xxhdpi;
    }

    public String getXxxhdpi() {
        return xxxhdpi;
    }

    public void setXxxhdpi(String xxxhdpi) {
        this.xxxhdpi = xxxhdpi;
    }

    public String getMdpi() {
        return mdpi;
    }

    public void setMdpi(String mdpi) {
        this.mdpi = mdpi;
    }

}
