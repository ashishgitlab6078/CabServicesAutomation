
package com.yatra.HourlySearchResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YatraCategory {

    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("seats")
    @Expose
    private Seats seats;
    @SerializedName("yatra_category_images")
    @Expose
    private YatraCategoryImages yatraCategoryImages;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Seats getSeats() {
        return seats;
    }

    public void setSeats(Seats seats) {
        this.seats = seats;
    }

    public YatraCategoryImages getYatraCategoryImages() {
        return yatraCategoryImages;
    }

    public void setYatraCategoryImages(YatraCategoryImages yatraCategoryImages) {
        this.yatraCategoryImages = yatraCategoryImages;
    }

}
