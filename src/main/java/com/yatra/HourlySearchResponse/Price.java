
package com.yatra.HourlySearchResponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("advance_charge")
    @Expose
    private AdvanceCharge advanceCharge;
    @SerializedName("total_charge")
    @Expose
    private TotalCharge totalCharge;
    @SerializedName("advance_percentage")
    @Expose
    private AdvancePercentage advancePercentage;
    @SerializedName("charges")
    @Expose
    private List<Charge> charges = null;
    @SerializedName("ecash")
    @Expose
    private Ecash ecash;

    public AdvanceCharge getAdvanceCharge() {
        return advanceCharge;
    }

    public void setAdvanceCharge(AdvanceCharge advanceCharge) {
        this.advanceCharge = advanceCharge;
    }

    public TotalCharge getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(TotalCharge totalCharge) {
        this.totalCharge = totalCharge;
    }

    public AdvancePercentage getAdvancePercentage() {
        return advancePercentage;
    }

    public void setAdvancePercentage(AdvancePercentage advancePercentage) {
        this.advancePercentage = advancePercentage;
    }

    public List<Charge> getCharges() {
        return charges;
    }

    public void setCharges(List<Charge> charges) {
        this.charges = charges;
    }

    public Ecash getEcash() {
        return ecash;
    }

    public void setEcash(Ecash ecash) {
        this.ecash = ecash;
    }

}
