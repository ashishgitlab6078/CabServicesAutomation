package com.yatra.CommonUtils;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class ExtentReport {

	final static Logger logger = Logger.getLogger(ExtentReport.class);
	public static ExtentReports extent;
	public static ExtentTest test;
	CommonFunctions cfObj = new CommonFunctions();
	public static int FailureCount;

	@BeforeSuite
	public void startReport() {
		FailureCount = 0;
		logger.info("Test suite execution started");
		extent = new ExtentReports(System.getProperty("user.dir") + "/test-output/AutomationReport.html", true);
		extent.addSystemInfo("HostName", "Vinod").addSystemInfo("Environment", "PROD");

		extent.loadConfig(new File(System.getProperty("user.dir") + "/resources/extentconfig.xml"));
	}

	@AfterMethod
	public void getResult(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			FailureCount++;
			test.log(LogStatus.FAIL, result.getThrowable());
		}
		extent.endTest(test);
	}

	@AfterSuite
	public void endReport() throws IOException, InterruptedException {
		logger.info("Test suite execution completed");
		LogManager.shutdown();

		extent.flush();
		extent.close();

		cfObj.renameLogFile();

//		 cfObj.sendTestExecutionReport("Test Cases - Flight End to End Flow Verification","Final Test Execution Report. Failed Cases:"+FailureCount, "vinod.kumar@yatra.com",
//		 "Controller Test Run - My Machine TEST");

		// cfObj.deleteReportFile();
	}

}
