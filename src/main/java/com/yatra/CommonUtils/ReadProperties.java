package com.yatra.CommonUtils;

/**@author - vinod.kumar
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadProperties {

	public static String readPropertyPath(String propertyKey) {

		Properties prop = new Properties();
		File dir1 = new File(".");
		String strBasePath = null;
		String val = null;
		String projectPath=null;
		try {
			strBasePath = dir1.getCanonicalPath();
			prop.load(new FileInputStream(strBasePath + File.separator + "resources"
					+ File.separator + "config.properties"));
			
			projectPath = System.getProperty("user.dir");
//			System.out.println(projectPath);
			
			val = prop.getProperty(propertyKey);
			val = projectPath+"\\"+val;
			
			// return val;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return val;
	}
	
	
	public static String readProperty(String propertyKey) {

		Properties prop = new Properties();
		File dir1 = new File(".");
		String strBasePath = null;
		String val = null;
		
		try {
			strBasePath = dir1.getCanonicalPath();
			prop.load(new FileInputStream(strBasePath + File.separator + "resources"
					+ File.separator + "config.properties"));
			
			val = prop.getProperty(propertyKey);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return val;

	}
	
	
}