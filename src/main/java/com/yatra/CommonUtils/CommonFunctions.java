package com.yatra.CommonUtils;

/**@author - vinod.kumar
*/



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.google.gson.Gson;
import com.yatra.loginBeans.*;

public class CommonFunctions {

	public String getApiResponseText(String apiUrl) throws IOException {
		String entireResponse = new String();
		try {

			URL url = new URL(apiUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			int responseCode = conn.getResponseCode();

			if (responseCode == 200) {
				Scanner scan = new Scanner(url.openStream());
				while (scan.hasNext())
					entireResponse += scan.nextLine();
				scan.close();
			}

			else {
				System.out.println("API Resonse Failure Code: " + responseCode);
				entireResponse = null;
			}

			System.out.println("\nFull API Response: " + entireResponse);
			conn.disconnect();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			entireResponse = null;
		}

		return entireResponse;
	}

	public String getApiResponseTextPostCall(String apiUrl, String sParams) throws IOException {
		String entireResponse = new String();
		try {


			URL url = new URL(apiUrl);

			URLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			((HttpURLConnection) conn).setRequestMethod("POST");

			conn.setReadTimeout(15000);
			conn.setConnectTimeout(15000);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(sParams);

			writer.flush();
			writer.close();
			os.close();

			int responseCode = ((HttpURLConnection) conn).getResponseCode();

			if (responseCode == 200) {
				String line;
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				while ((line = br.readLine()) != null) {
					entireResponse += line;
				}
			} else {
				System.out.println("API Resonse Failure Code: " + responseCode);
				entireResponse = null;
			}

			System.out.println("\nFull API Response: " + entireResponse);

			((HttpURLConnection) conn).disconnect();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			entireResponse = null;
		}

		return entireResponse;
	}
	
	
	public String getApiResponseTextPostCall(String apiUrl, String sParams,String[][] headers) throws IOException {
		String entireResponse = new String();
		try {

			System.out.println("API url "+apiUrl);
			System.out.println("sParams"+sParams);
			
			System.out.println(headers.length);
			
			
			URL url = new URL(apiUrl);

			URLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			((HttpURLConnection) conn).setRequestMethod("POST");
			
			
			for(int i=0;i<headers.length;i++)
			{
				conn.setRequestProperty(headers[i][0],headers[i][1]);
				
				System.out.println(headers[i][0]+"  "+headers[i][1]);
			}
			
			conn.setReadTimeout(15000);
			conn.setConnectTimeout(15000);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			
			conn.setRequestProperty("Content-Type", "application/json; charset=utf8");
		    conn.setRequestProperty("Accept", "application/json");
			OutputStream os = conn.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(sParams);

			writer.flush();
			writer.close();
			os.close();

			//String response=((HttpURLConnection) conn).getResponseMessage();
			///System.out.println(response);
			
			//int responseCode = ((HttpURLConnection) conn).getResponseCode();

			///if (responseCode == 200) {
				String line;
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				while ((line = br.readLine()) != null) {
					entireResponse += line;
				}
			//} else {
				//System.out.println("API Resonse Failure Code: " + responseCode);
				//entireResponse = null;
			//}

			System.out.println("\nFull API Response: " + entireResponse);

			((HttpURLConnection) conn).disconnect();

		} catch (Exception e) {
			System.out.println(e);
			entireResponse = null;
		}

		return entireResponse;
	}

	public boolean renameLogFile() throws FileNotFoundException {
		boolean result = false;

		try {
			String logPath = ReadProperties.readPropertyPath("logsPath");

			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			Calendar cal = Calendar.getInstance();

			String sReportDate = dateFormat.format(cal.getTime()).replaceAll(":", "");

			File dir = new File(logPath);
			File[] files = dir.listFiles();
			if (files == null || files.length == 0) {
				System.out.println("No Log file exist on given path: " + logPath);
			} else {
				for (int i = 0; i < files.length; i++) {
					if (files[i].getName().contains("testlog.log")) {
						File oldfile = new File(logPath + "testlog.log");
						Thread.sleep(500);
						File newFile = new File(logPath + "logs " + sReportDate + ".log");
						Thread.sleep(500);
						oldfile.renameTo(newFile);
						result = true;
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void sendMailForTestFailure(String testCase, String superpnr, String testData, String mesg, String strTo,
			String subject) throws IOException, InterruptedException {

		// final String user = "yatratestbookings";//change accordingly
		// final String password = "Password@1";//change accordingly
		final String user = "yatratestbookings@gmail.com";// change accordingly
		final String password = "yatra123";// change accordingly

		String to = strTo;
		// String host = "smtp1.yatra.in";
		String host = "smtp.gmail.com";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		// props.put("mail.smtp.port", "25");

		// for outside network mail
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		});

		// Compose the message
		try {

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			// message.addRecipient(Message.RecipientType.TO, new
			// InternetAddress("saurabh.suman@yatra.com"));

			message.setSubject(subject);
			message.setText(mesg);

			// creates message part
			MimeMultipart multipart = new MimeMultipart();

			// Body part with multiple message
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(testCase, "text/html");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent("<BR>" + superpnr, "text/html");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent("<BR>" + testData, "text/html");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent("<BR>" + mesg, "text/html");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent("<BR>" + "----------------Thanks !!----------------", "text/html");
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);
			// send the message
			Transport.send(message);

			System.out.println();
			System.out.println("Mail sent successfully...");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void sendTestExecutionReport(String testCase, String mesg, String strTo, String subject)
			throws IOException, InterruptedException {

		// final String user = "yatratestbookings";//change accordingly
		// final String password = "Password@1";//change accordingly
		final String user = "yatratestbookings@gmail.com";// change accordingly
		final String password = "yatra123";// change accordingly

		String to = strTo;
		// String host = "smtp1.yatra.in";
		String host = "smtp.gmail.com";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		// props.put("mail.smtp.port", "25");

		// for outside network mail
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		});

		// Compose the message
		try {

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("gaurav.goel@yatra.com"));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("saurabh.suman@yatra.com "));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("abhishek.jain1@yatra.com "));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("venkat.gadepalli@yatra.com "));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("sneha.jha@yatra.com"));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("rashi.agarwal@yatra.com"));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("vivek.parihar@yatra.com"));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("akshay.nagpurkar@yatra.com"));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("priyang.agarwal@yatra.com"));
			
			message.setSubject(subject);
			message.setText(mesg);

			// creates message part
			MimeMultipart multipart = new MimeMultipart();

			// Body part with multiple message
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(testCase, "text/html");
			multipart.addBodyPart(messageBodyPart);

			// messageBodyPart = new MimeBodyPart();
			// messageBodyPart.setContent("<BR>" + superpnr, "text/html");
			// multipart.addBodyPart(messageBodyPart);
			//
			// messageBodyPart = new MimeBodyPart();
			// messageBodyPart.setContent("<BR>" + testData, "text/html");
			// multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent("<BR>" + mesg, "text/html");
			multipart.addBodyPart(messageBodyPart);

			/*
			 * messageBodyPart = new MimeBodyPart();
			 * messageBodyPart.setContent("<BR>" +
			 * "----------------Thanks !!----------------", "text/html");
			 * multipart.addBodyPart(messageBodyPart);
			 */

			// creates attach part
			if (getReportFilePath() != null) {
				MimeBodyPart attachPart = new MimeBodyPart();
				try {
					attachPart.attachFile(getReportFilePath());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				multipart.addBodyPart(attachPart);
			} else {
				messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent("<BR>" + "Sorry! somthing went wrong, report NOT generated.", "text/html");
				multipart.addBodyPart(messageBodyPart);
			}
			message.setContent(multipart);
			// send the message
			Transport.send(message);

			System.out.println("\nFinal Report Mail sent successfully...");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public File getReportFilePath() {
		File reportFile = null;
		try {
			File dir = new File(ReadProperties.readPropertyPath("reportPath"));

			File[] files = dir.listFiles();
			if (files == null || files.length == 0) {
				System.out.println("No Report exist on given path: " + ReadProperties.readPropertyPath("reportPath"));
			} else {
				for (int i = 0; i < files.length; i++) {
					if (files[i].getName().contains("AutomationReport.html")) {
						reportFile = files[i];
					}
				}
			}
		} catch (Exception e) {

		}
		return reportFile;
	}

	public void deleteReportFile() {
		try {
			File dir = new File(ReadProperties.readPropertyPath("reportPath"));

			File[] files = dir.listFiles();
			if (files == null || files.length == 0) {
				System.out.println("No Report exist on given path: " + ReadProperties.readPropertyPath("reportPath"));
			} else {
				for (int i = 0; i < files.length; i++) {
					if (files[i].getName().contains("AutomationReport.html")) {
						files[i].delete();
					}
				}
			}
		} catch (Exception e) {

		}
	}
	
	 public static String entireLoginRespnse;
	 public static LoginInfo logininfo;
		public static ArrayList<String> mbLogInErrorList=new ArrayList<String>();
		public boolean mbLogin()
		{
			boolean result=false;
			//LoginInfo logininfo=null;
			CommonFunctions cfobj=new CommonFunctions();

			String userEmail=Constants.MBEMAILID;
			String userPassword=Constants.MBPASSWORD;
			String server=ReadProperties.readProperty("controllerServer");
			String loginUrl=server+"//ccwebapp/mobile/common/mcommonandroid/login.htm";
			String data="osVersion=21&"
					+ "authProvider=YATRA&deviceId=54ce88d311ff9aa&"
					+ "appVersion=138"+"&email="+userEmail+"&password="+userPassword;
			
			//System.out.println(loginUrl);
			//System.out.println(data);
			
			try{
			entireLoginRespnse=cfobj.getApiResponseTextPostCall(loginUrl,data);
			logininfo=new Gson().fromJson(entireLoginRespnse, LoginInfo.class);
			if(!((logininfo.getResCode().toString().equals("200")&&(logininfo.getResMessage().toString().equals("Success")))))
			{
				mbLogInErrorList.add(logininfo.getResMessage());
				return result=false;
			}
		  
			if(logininfo.getResponse().getAuthToken()!="" && logininfo.getResponse().getAuthToken()!=null)
			{
				result=true;
			}
			
			}catch(Exception e)
			{
				e.getMessage();
				mbLogInErrorList.add(e.getMessage());
			}
			return result;		
		}
		
		public String getSSOToken()
		{
			String SSOToken="";
			CommonFunctions cfobj=new CommonFunctions();

			cfobj.mbLogin();
			
			SSOToken=logininfo.getResponse().getAuthToken();
			
			return SSOToken;
		}
		
		
		
		public long getEpochTime(int noOfdaysFromCurrentDate)
		{
			long ePochTime=0;
			String departureDate="";
			try {
				Date date = new Date(); // Start date
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				departureDate = simpleDateFormat.format(date);
				Calendar c = Calendar.getInstance();
				c.setTime(simpleDateFormat.parse(departureDate));
				c.add(Calendar.DATE, noOfdaysFromCurrentDate);
				ePochTime=c.getTimeInMillis();
				departureDate = simpleDateFormat.format(c.getTime());
			} catch (Exception e) {
				e.printStackTrace();
				

			}
			System.out.println("ePochTime "+ePochTime);
			return ePochTime;
		}

}