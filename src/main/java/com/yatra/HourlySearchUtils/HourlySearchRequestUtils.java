package com.yatra.HourlySearchUtils;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.yatra.CommonUtils.CommonFunctions;
import com.yatra.HourlySearchRequest.HourlySearchRequestBean;
import com.yatra.HourlySearchResponse.HourlySearchResponse;

public class HourlySearchRequestUtils {
	
	public String createRequest(HourlySearchRequestBean hourlySearchRequestBean)
	{
		String requestJson="";
		
		JSONObject pickUp=new JSONObject();
		pickUp.put("address",hourlySearchRequestBean.getAddress());
		pickUp.put("city_id",hourlySearchRequestBean.getCity_id());
		
		JSONObject query=new JSONObject();
		query.put("pickup",pickUp);
		query.put("travel_type",hourlySearchRequestBean.getTravel_type());
		query.put("seats",hourlySearchRequestBean.getSeats());
		query.put("pickup_time",hourlySearchRequestBean.getPickup_time());
		query.put("driver_criteria",hourlySearchRequestBean.getDriver_criteria());
		query.put("vehicle_criteria",hourlySearchRequestBean.getVehicle_criteria());
		query.put("hourly_package_id",hourlySearchRequestBean.getHourly_package_id());
		JSONObject requestJsonObject=new JSONObject();
		requestJsonObject.put("query",query);
		System.out.println("Request Object"+requestJsonObject.toString());
		requestJson=requestJsonObject.toString();		
		return requestJson;
	}
	
	public boolean hourlySearchResultVerifcation(String requestString)
	{
		boolean result=false;
		CommonFunctions cfobj=new CommonFunctions();
		String apiUrl="https://secure.yatra.com/cabs/v1.2/search";
		String sParams=requestString;
		String headers[][]=getHeaders();
		try{
			String entireSearchResponse=cfobj.getApiResponseTextPostCall(apiUrl, sParams, headers);
			
			HourlySearchResponse hourlySearchResponse=new Gson().fromJson(entireSearchResponse,HourlySearchResponse.class);
			
			System.out.println("Search_Id"+hourlySearchResponse.getSearchId());
			
			}catch(Exception e)
		{
			System.out.println(e);
		}
		
		
		return result;
		
	}
	
	public String[][] getHeaders()
	{
		String[][] headers=new String[9][2];
		
		headers[0][0]="X-YT-SESSION-ID";
		headers[0][1]="df58089e-d43e-4a73-a6e2-8b2906dac154";
		
		headers[1][0]="X-YT-INTERACTION-ID";
		headers[1][1]="cd41597f-2bb9-43a1-a514-29f7c9f1296e";
		
		headers[2][0]="X-YT-APP-VERSION";
		headers[2][1]="12.1.1";
		
		headers[3][0]="X-YT-APP-VERSION_CODE";
		headers[3][1]="172";
		
		headers[4][0]="X-YT-OS-VERSION";
		headers[4][1]="22";
		
		headers[5][0]="X-YT-DEVICE-MODEL";
		headers[5][1]="SM-J200G";
		
		headers[6][0]="X-YT-DEVICE-BRAND";
		headers[6][1]="samsung";
	
		headers[6][0]="YT-CHANNEL";
		headers[6][1]="ANDROID";
		
		headers[7][0]="YT-TENANT-CODE";
		headers[7][1]="mhourlyandroid";
		
		
		
		
		CommonFunctions cfobj=new CommonFunctions();
		String ssoToken=cfobj.getSSOToken();
		System.out.println("SSo Token "+ssoToken);
	
		headers[8][0]="YT-SSO-TOKEN";
		headers[8][1]=ssoToken;
		
		return headers;
		
//		X-YT-SESSION-ID: df58089e-d43e-4a73-a6e2-8b2906dac154
//		X-YT-INTERACTION-ID: cd41597f-2bb9-43a1-a514-29f7c9f1296e
//		X-YT-APP-VERSION: 12.1.1
//		X-YT-APP-VERSION_CODE: 172
//		X-YT-OS-VERSION: 22
//		X-YT-DEVICE-MODEL: SM-J200G
//		X-YT-DEVICE-BRAND: samsung
//		YT-SSO-TOKEN: 1f0b0357-4c76-4f03-83c4-0cbe15a97f5f
//		YT-CHANNEL: ANDROID
//		YT-TENANT-CODE: http://172.16.6.174:8090/cabs/v1.2/search
		
		///return headers;
	}

}
